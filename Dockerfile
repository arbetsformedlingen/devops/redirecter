FROM nginx:1.25.4-alpine3.18-slim

COPY nginx/conf.d/default.conf /etc/nginx/conf.d
COPY nginx/nginx.conf /etc/nginx/

EXPOSE 8080
